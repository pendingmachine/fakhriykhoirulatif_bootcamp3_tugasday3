import "./App.css";

import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";

import ButtonAppBar from "./component/ButtonAppBar";

function App() {
  const [users, setUsers] = useState([]);
  const [userData, setUserData] = useState({
    name: "",
    address: "",
    hobby: "",
  });

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const addUser = () => {
    setUsers(users.concat(userData));
    handleClose();
  };

  const updateUserData = (key, value) => {
    const newUserData = userData;
    newUserData[key] = value;
    setUserData(newUserData);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  // Reset user data when the dialog is opened or closed
  useEffect(() => {
    setUserData({
      name: "",
      address: "",
      hobby: "",
    });
  }, [open]);

  return (
    <div>
      <ButtonAppBar onButtonClick={handleClickOpen} />

      {users.length ? (
        <Stack
          spacing="4px"
          sx={{
            padding: "4px",
            backgroundColor: "black",
          }}
        >
          {users.map((user, index) => (
            <div className="user" key={index}>
              <div>
                <div className="name">{user.name}</div>
                <div>{user.address}</div>
              </div>
              <div className="hobby">{user.hobby}</div>
            </div>
          ))}
        </Stack>
      ) : (
        <div className="no-user">
          <div className="zero">0</div>
          <div className="text">User</div>
        </div>
      )}

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle
          sx={{
            textAlign: "center",
          }}
        >
          Add User
        </DialogTitle>
        <DialogContent>
          <Stack
            component="form"
            noValidate
            autoComplete="off"
            spacing={2}
            sx={{
              padding: "10px 0",
              width: "300px",
            }}
          >
            <TextField
              label="Name"
              onChange={(e) => updateUserData("name", e.target.value)}
            />
            <TextField
              label="Address"
              onChange={(e) => updateUserData("address", e.target.value)}
            />
            <TextField
              label="Hobby"
              onChange={(e) => updateUserData("hobby", e.target.value)}
            />
          </Stack>
        </DialogContent>
        <DialogActions
          sx={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button onClick={addUser} variant="contained">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default App;
